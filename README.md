# OCR_placas_veiculos_tesseract

**Pré requisitos**

```
Python3 
Pip3
Tesseract  git clone https://github.com/tesseract-ocr/tesseract.git
```

**Depêndencias do projeto (instalar)**

```
pip3 install pytesseract
pip3 install opencv-python
pip3 install easygui
pip3 install numpy 
```

**Executar Script**
```
python ocr_tesseract.py
```
ou 

```
python3 ocr_tesseract.py
```

Selecione uma imagem de placa e veja o resultado do promt.

**O que é implementado neste projeto?**

A transcrição de placas de veículo (OCR de placas de veículo)

A transcrição será realizada para detectar veículos com alguma irregularidade automaticamente utilizando outros projetos em desenvolvimento.
O processo futuramente se dará da seguinte forma: 

Filmagem > Detecção do veículo e da placa > Transcrição da placa > Busca pela placa no sistema > Identificação do nível irregularidade > Emissão de alerta caso haja irregularidade
 
O OCR é uma sigla do inglês que significa Optical Character Recognition - Reconhecimento óptico de caracteres, seu papel é converter imagens de textos em textos reais para serem compreendidos como caracteres ASCII; 
 
**Objeto de estudo - Tesseract OCR** 

O Tesseracr OCR é um software de reconhecimento ótico de caracteres de código aberto, originalmente desenvolvido pela Hewlett-Packard e foi por um tempo mantido pelo Google e atualmente está hospedado no GitHub para ser usado em diversos tipos de transcrição (imagem para texto).
A ferramenta tornou-se disponível para uso open source em 1994 e foram feitas diversas melhorias desde então e sua versão mais estável é de 2019, baseada em LSTM - Long Short Term Memory uma arquitetura de rede neural recorrente artificial usada no campo do aprendizado profundo.
 
**Como funciona o tesseract?**

O Tesseract faz a transcrição baseando-se nos seus arquivos de treinamento, ela possui um treino de caracteres desenvolvido pela google com diversos idiomas na fonte Times Roman e com base nesse treino é feita a transcrição das palavras de uma imagem para texto. 
Para que essa transcrição ocorra sem falhas, todo o fundo da imagem deve ser removido utilizando técnicas de processamento de imagem, caso queira fazer a transcrição de um PDF por exemplo, nenhum tratamento prévio precisa ser realizado porque o mesmo não possui fundo e a transcrição sairá idêntica ao texto da imagem.
 
**Desafios da transcrição de placas de veículo**

Placas de veículos são imagens reais,  sofrem interferências de iluminação, ruídos diversos no objeto (caracteres apagados), ruídos na filmagem (imagem de movimento), angulação da imagem, entre outros tipos de ruído.
Outro desafio está quanto a origem da placa, entre elas estão: 
-     Placas Antigas do Brasil;
-     Novas Placas da tríplice;
-     Placas Antigas do Paraguai;
-     Placas Antigas do Brasil.

    
Estas apresentam  diferenças de fontes, cores e contraste. 
 
Para que a transcrição ocorra de forma precisa, todos esses desafios devem ser abordados com processamento de imagem de forma que os caracteres das placas sejam evidenciados. Além disso, para que não haja confusão durante a transcrição com caracteres muito parecidos, como por exemplo a antiga placa do Brasil, onde o caracter I e 1 são idênticos na fonte, deve ser realizado um treinamento preciso do tesseract com diversos exemplos de placas. 
 
**O que já foi implementado?**
 
O script de processamento de imagens atualmente está voltado para placas da tríplice, sem grandes angulações e com uma iluminação mediana; o script faz transcrições com outros tipos de placas, mas sem precisão, esta é a primeira versão. 

**O que é esperado deste projeto?**

Conforme o detector de veiculos e placas faz a detecção, o mesmo recorta a imagem da placa e a envia para um diretório, este script irá detectar uma nova imagem assim que ela chegar, fazer a transcrição da placa e enviar o que foi transcrito para o sistema que irá verificar irregularidades.





 


 

