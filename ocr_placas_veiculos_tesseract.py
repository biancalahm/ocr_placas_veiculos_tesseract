# test file if you want to quickly try tesseract on a license plate image
import pytesseract
import pytesseract as ocr
import cv2
import os
import easygui
# Imports PIL module 
#from PIL import Image
import numpy as np

# If you don't have tesseract executable in your PATH, include the following:
# pytesseract.pytesseract.tesseract_cmd = r'<full_path_to_your_tesseract_executable>'
# Example tesseract_cmd = r'C:\Program Files (x86)\Tesseract-OCR\tesseract'

# point to license plate image (works well with custom crop function)

   # from tkinter import Tk for Python 3.x



filename = (easygui.fileopenbox())
gray = cv2.imread(filename, 0)
# clahe = cv2.createCLAHE(clipLimit=3., tileGridSize=(8,8))
# lab = cv2.cvtColor(gray, cv2.COLOR_BGR2LAB)  # convert from BGR to LAB color space
# l, a, b = cv2.split(lab)  # split on 3 different channels
# l2 = clahe.apply(l)  # apply CLAHE to the L-channel

# lab = cv2.merge((l2,a,b))  # merge channels
# img2 = cv2.cvtColor(lab, cv2.COLOR_LAB2BGR)
# cv2.imshow("sharpened", img2)
# cv2.waitKey(0)
#aumenta o contraste e consequentemente acentua os caracteres da placa
kernel_sharpeing_1 = np.array([[-1, -1, -1],[-1, 9, -1],[-1,-1,-1]])
sharpened = cv2.filter2D(gray,-1,kernel_sharpeing_1)
'''cv2.imshow("sharpened", sharpened)
cv2.waitKey(0)'''
#amplia a imagem da placa 
gray = cv2.resize( sharpened, None, fx = 3, fy = 3, interpolation = cv2.INTER_LINEAR)
# binariza a imagem 
ret, thresh = cv2.threshold(gray, -55, 255, cv2.THRESH_OTSU | cv2.THRESH_BINARY_INV)
'''cv2.imshow("Otsu", thresh)
cv2.waitKey(0)'''
#reforca novamente as linhas da imagem binarizada 
#cv2.imshow("sharpened", sharpened)
#cv2.waitKey(0)
# aplicando morfologia matematica na imagem 
#tecnica de abertura 
kernel = np.ones((5,5),np.uint8)
rect_kern = cv2.getStructuringElement(cv2.MORPH_CROSS, (3,3))
opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel)
#aplicando a erosao na imagem para afinar os caracteres 
erosao = cv2.erode(opening, rect_kern, iterations = 1)
#Aplicando fechamento para alinhamento ds letras 
#closing = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel)
closing = erosao
'''cv2.imshow("closing", closing)
cv2.waitKey(0)'''
# find contours  (deteccao de contornos) #
try:
    contours, hierarchy = cv2.findContours(closing, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
except:
    ret_img, contours, hierarchy = cv2.findContours(closing, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

#transformacao di contorno para um retangulo 
sorted_contours = sorted(contours, key=lambda ctr: cv2.boundingRect(ctr)[0])

#copia para ser exibida no fim do processamento
im2 = closing.copy()
#inicializando variaveis 
plate_num_roi= ""
plate_num_dilate = ""
plate_num_erode = ""
plate_num = ""
cont = 0
confidence_final =""
#Base que sera usada futuramente para o processamento de caracteres com contorno pouco definido 
#eng = base criada pela google com palavras no idioma ingles e fonte Times 
#num = treino parcial com letras do alfabeto com as fontes utilizadas pelas placas da triplice e placas antigas do Brasil 
#caracter = treino parcial baseado em letras binarizadas processadas e depois coletadas para facilitar o reconhecimento pelo tesseract 
plate_num = pytesseract.image_to_data(closing, config='-c  tessedit_char_whitelist=0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ --psm 6 --oem 1', lang='num+eng+caracteres')
print('BASE ')
print(plate_num) #img8 #img9 #ACU247 #ASF2878

#processamento para transcricao da palavra completa (algumas vezes o tesseract tem mais facilidade de entender um conjunto de caracteres do que caracteres individuais)
''' erosao_base = cv2.erode(closing, rect_kern, iterations = 1) 
 plate_num = pytesseract.image_to_data(erosao_base, config='-c  tessedit_char_whitelist=0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ --psm 6 --oem 1', lang='num+eng+caractere')
 print('BASE_erosao ')
 print(plate_num) #img3 #AY(V)G3473
 dilate_base = cv2.dilate(closing, rect_kern, iterations = 1) 
 plate_num = pytesseract.image_to_data(dilate_base, config='-c   tessedit_char_whitelist=0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ --psm 1 --oem 1', lang='num+eng+caractere')
'''
# print('BASE_dilate ')
# print(plate_num) #img1 #AGZ6H83
# variavel para o calculo total da confianca da deteccao baseado nos resultados gerados pelo tesseract e outras metricas
confidence_final_confianca = 0.0
for cnt in sorted_contours:
    confidence = ""
    x,y,w,h = cv2.boundingRect(cnt)
    rect = cv2.minAreaRect(cnt)
    #outras formas de definicao e calculo contorno 
    ''' 
    box = cv2.boxPoints(rect)
    box = np.int0(box)
    '''
    # procurar solucao para o drawContours que nao seja box
    cv2.drawContours(closing,[cnt],0,(0,255,0),1)
    #cv2.imshow("drawContours", erosao)
    #cv2.waitKey(0)
    height, width = closing.shape
    # if height of box is not a quarter of total height then skip
    if height / float(h) > 6: continue
    ratio = h / float(w)
    # if height to width ratio is less than 1.5 skip
    if ratio < 1.5: continue
    area = h * w
    # if width is not more than 25 pixels skip
    if width / float(w) > 25: continue
    # if area is less than 100 pixels skip
    if area < 100: continue
    # draw the rectangle
    #rect = cv2.rectangle(im2, (x,y), (x+w, y+h), (0,255,0),2)
    roi = closing[y-5:y+h+5, x-5:x+w+5]
    #roi = cv2.bitwise_not(roi)
    cont = cont + 1
    #print('interacao = ',cont)
    '''cv2.imshow("roi", roi)
    cv2.waitKey(0)'''
    print('confidence')
    #primeira tentativa de transcricao - mais adequada para imagens de luninosidade media 
    text_roi = pytesseract.image_to_data(roi, config='-c  tessedit_char_whitelist=0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ --psm 6 --oem 1', lang='eng+num+caracteres')
    text_roi = text_roi.replace('\t',',').replace('\n','').split(',')
    #print(text_roi)
    text_roi=text_roi[-2:]
    print(text_roi)
    if(float(text_roi[0]) > 60):
        confidence =text_roi
    
    dilate = cv2.dilate(roi, rect_kern, iterations = 2)
    '''cv2.imshow("roi", dilate)
    cv2.waitKey(0)'''
    ##segunda tentativa de transcricao - mais adequada para imagens pouca sombra,
    # pois a alta luminosidade deixa os caracteres bem  mais finos e o dilate tem o papel 
    # de engrossar para tentar uma deteccao mais precisa  
   
    text_dilate = pytesseract.image_to_data(dilate, config='-c  tessedit_char_whitelist=0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ --psm 6 --oem 1', lang='eng+num+caracteres')
    text_dilate = text_dilate.replace('\t',',').replace('\n','').split(',')
    text_dilate=text_dilate[-2:]
    print(text_dilate)
    if(float(text_dilate[0]) > 60 and float(text_dilate[0]) > float(text_roi[0]) ):
       confidence =text_dilate
    #print(text_dilate)
    erode = cv2.erode(roi, rect_kern, iterations = 1)
    '''cv2.imshow("roi", opening)
    cv2.waitKey(0)'''
    ##terceira tentativa de transcricao - mais adequada para imagens muita sombra,
    # pois a baixa luminosidade deixa os caracteres bem  mais grossos e pouco definidos 
    # e o erode tem o papel afina-lo para tentar uma deteccao mais precisa  
    text_erode = pytesseract.image_to_data(erode, config='-c  tessedit_char_whitelist=0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ --psm 6 --oem 1', lang='eng+num+caracteres')
    text_erode = text_erode.replace('\t',',').replace('\n','').split(',')
    text_erode=text_erode[-2:]
    print(text_erode)
    if(float(text_erode[0]) > 60 and float(text_erode[0]) > float(text_dilate[0]) and float(text_erode[0]) > float(text_roi[0])):
       confidence =text_erode
    print("confidence "+str(confidence))
    if not(confidence):
        
        lista = text_erode[1]+text_roi[1]+text_dilate[1]

        lista.replace('','-1')
        a= lista.count(text_erode[1])
        b = lista.count(text_roi[1])
        c= lista.count(text_dilate[1])
        print(a,b,c)
        if(a>=b or a >=c ):
            confidence =text_erode
        if(b>=a or b >=c ):
            confidence =text_roi
        if(c>=a or c >=b ):
            confidence =text_dilate
        print(str(confidence))
        print("____________________________________________________________________________________________________")
    confidence_final_confianca += float(confidence[0])
    confidence_final += confidence[1]
    confidence_final_confianca = confidence_final_confianca
print(len(confidence_final))
if(len(confidence_final) != 7):
   print(" IMPORTANTE : A transcricao nao esta completa !")
print("resultado obtido = ", confidence_final)
cv2.imshow("Character's Segmented ", im2)
cv2.waitKey(0)
cv2.destroyAllWindows()
